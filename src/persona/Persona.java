/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persona;

/**
 *
 * @author Hernando Vasquez Izquierdo
 */
/*Clase que funciona como array de objetos, el codigo comentado son metodos 
para modificar el contenido  */
public class Persona
    {
    
    private char nombre[];
    private int numero;
/*Constucror que permite crear los tipo Persona*/
    public Persona(char[] nombre, int numero)
        {
        this.nombre = nombre;
        this.numero = numero;
        }

    public char[] getNombre()
        {
        return nombre;
        }

//    public void setNombre(char[] nombre)
//        {
//        this.nombre = nombre;
//        }

    public int getNumero()
        {
        return numero;
        }

//    public void setNumero(int numero)
//        {
//        this.numero = numero;
//        }
   
    }
