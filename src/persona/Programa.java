/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persona;

/**
 *
 * @author Hernando Vasquez Izquierdo
 */
public class Programa
    {
    /*Se declara el puntero del array Personas para poder usarlo con posteridad 
    a demas de atributos necesarios*/
    Persona[] persona;
    int numArray;
    LT manipular = new LT();

    public void inicio()
        {
        System.out.print("Introdusca la cantidad de elementos a tratar: ");
       /*llama metodo para crear array del tipo Persona segun el tamaño 
        designado por el usuario*/
        persona = new Procesos().pedirPersonas(manipular.llegirSencer());
        System.out.print("Que nombre quieres calcular?: ");
        /*envia variable(Puntero) del tipo Persona para tratar el contenido 
        segun lo espesicicado y realizar salida a pantalla */
        new Procesos().tratarPersona(persona, manipular.llegirLinia().toCharArray());
        }

    public static void main(String[] args)
        {
        new Programa().inicio();
        }
    }
