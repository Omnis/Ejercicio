/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persona;

/**
 *
 * @author Y
 */
/*Clase encargada de los procesos a realizar como peticiones de busquedas o 
recorridos o tratamientos de Arrays*/
public class Procesos
    {

    private LT manipular = new LT();
    private char[] linea;
    private int total;

    /*recibe como lvalor el maño total que tendra el Array e instancia los objetos 
    dentro del Array de objetos Personas*/
    public Persona[] pedirPersonas(int numArray)
        {
        /*crea array segun el tamaño que le pida el ususario*/
        Persona dato[] = new Persona[numArray];
        /*bucle para crear las instacias de los objetos, se crea a medida 
        que el usuario los rellena*/
        for (int i = 0; i < dato.length; i++) {
            System.out.print("Nombre " + (i + 1) + ": ");
            linea = (manipular.llegirLinia()).toCharArray();
            System.out.print("Cantidad " + (i + 1) + ": ");
            int num = manipular.llegirSencer();
            dato[i] = new Persona(linea, num);
        }
        return dato;
        }

    /*reliza unn recorrido por el Array de objetos e imprime resultado*/
    public void tratarPersona(Persona[] dato, char[] nombre)
        {
        total = 0;
        /*variable auxiliar para determinar si ha encontrado algun registro*/
        boolean aux = false;
        for (int i = 0; i < dato.length; i++) {
            /*llama a metodo hayPersonas( array de carracters del nombre pedido
            , da el objeto del Array de Persona en la posicion indicada) */
            if (hayPersona(nombre, dato[i])) {
                aux=true;
                total = total + dato[i].getNumero();
            }
        }
        if (!aux) {
            System.out.println("no hay coincidencia en el registro");
        } else {
            System.out.println("El resultat de la suma de " + imprimir(nombre) + " és: " + total);
        }
        }

    /*Busca si  hay coinsidencia en el nombre dado(array de caracteres) a buscar y el dado por el 
    array de objetos(solo recibe el objeto de Persona, no el array)*/
    private boolean hayPersona(char[] nombre, Persona dato)
        {
        boolean aux = true;
        /*obtine el array de caractes del objeto dato que a su vez es del tipo 
        Persona y que contine la inforcacion*/
        linea = dato.getNombre();
        if (nombre.length == dato.getNombre().length) {
            for (int i = 0; i < nombre.length && (aux); i++) {
                if (nombre[i] != linea[i]) {
                    aux = false;
                }
            }
        } else {
            aux = false;
        }
        return aux;
        }
    /*metodo para con vertir el array de caracteres en un string 
    para poder imprimirlo*/
    public String imprimir(char[]nombre)
        {
        String nom="";
        for (int i = 0; i < nombre.length; i++) {
            nom = nom+ nombre[i];
        }
        return nom;
        }
    }
